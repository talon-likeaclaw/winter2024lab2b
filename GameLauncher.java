import java.util.Scanner;

public class GameLauncher {
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int gameSelector = 0;
		
		// Greet the user and ask them which game they would like to play
		System.out.println("Hello! What game would you like to play?");
		
		// If the user does not input 1 or 2 it will prompt them again until they input 1 or 2
		while (gameSelector < 1 || gameSelector > 2) {
			System.out.println("Type 1 to play Hangman or type 2 to play Wordle");
			gameSelector = reader.nextInt();
		}
		
		if (gameSelector == 1) {
			// Ask the user to input a 4 letter word to be guessed.
			System.out.println("Give me a 4 letter word with no repeating characters");
			String wordInput = reader.next();
			Hangman.runGame(wordInput);
		}
		
		if (gameSelector == 2) {
			// Start the wordle game
			Wordle.runGame(Wordle.generateWord());
		}
	}
	
}