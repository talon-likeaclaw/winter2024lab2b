/*
 * Assignment 2  2023 - 110
 * Program that runs a simple game of Hangman from user input.
 * @author Talon Dunbar - 2131651
 * @version 2023-11
 */
public class Hangman {
	
	/*
	 * Method that checks to see if the character is within the word.
	 * Returns the position of the character in the input word, or -1 if not in input word.
	 */
	public static int isLetterInWord(String wordInput, char characterGuessed) {
		// Convert guess char to uppercase
		characterGuessed = toUpperCase(characterGuessed);
		
		for (int i = 0; i < wordInput.length(); i++) {
			// Converts character in input word to uppercase before comparing with user's guess
			if (toUpperCase(wordInput.charAt(i)) == characterGuessed) {
				return i; // Returns the index number, if character resides within the word at index.
			}
		}
		return -1; // Return -1 if character is not within word.
	}
	
	/*
	 * Method that converts lowercase letters to uppercase.
	 * Returns the uppercase version of any letter.
	 */
	public static char toUpperCase(char characterGuessed) {
		// Substracts 32 if character is lowercase to convert to uppercase via ascii code.
		if (characterGuessed >= 'a') {
			characterGuessed -= 32;
			return characterGuessed;
		} else {
			return characterGuessed;
		}
	}
	/*
	 * Method that prints the progress of the characters that have been guess correctly.
	 * Does not return anything. Prints to the console instead.
	 */

	public static void printWork(String wordInput, boolean[] letterInWord) {
		// Defines '_' char variables for each letter.
		char[] charArray = new char[] {'_','_','_','_'};
		/* 
		* Checks to see if boolean values have become true.
		* If true, the '_' char variable is written as the character at the index number of the input String.
		*/
		for (int i = 0; i < 4; i++) {
			if (letterInWord[i]) {
				charArray[i] = wordInput.charAt(i);
			}
		}
		// Prints the results.
		System.out.println("Your result is " + charArray[0] + " " + charArray[1] + " " + charArray[2] + " " + charArray[3]);
	}
	
	/*
	* Method that runs the game using the other methods and provides end game states.
	* Keeps track of the boolean state of each letter.
	* Keeps track of how many correct and wrong guesses have been made.
	*/
	public static void runGame(String wordInput) {
		
		boolean [] letterInWord = new boolean[] {false, false, false, false};
		int loseCount = 6, winCount = 4;
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		// Checks to see if game is over or not.
		while (loseCount > 0 && winCount > 0) {
			// Asks user for input guess character.
			System.out.println("Enter a letter: ");
			char characterGuessed = reader.next().charAt(0);
			
			// Boolean value is changed to true at correct index if user guess is in word.
			for (int i = 0; i < 4; i++) {
				if (isLetterInWord(wordInput, characterGuessed) == i) {
					letterInWord[i] = true;
					winCount--;
				} 
			}
			// If the guessed char is not in the word the user is one guess closer to losing.
			if (isLetterInWord(wordInput, characterGuessed) == -1) {
				loseCount--;
			}
			
			// Provides user with the state of the game.
			printWork(wordInput, letterInWord);
			
			// Provides user with continue, win, and lose game states and feedback.
			// Continue playing the game and inform user how many guesses they have remaining. 
			if (loseCount > 0 && winCount > 0) {
				System.out.println("You have " + loseCount + " guesses remaining.");
			} 
			// User lost the game.
			else if (loseCount == 0 & winCount > 0) {
				System.out.println("Better luck next time.");
			} 
			// User won the game.
			else if (loseCount > 0 && winCount == 0) {
				System.out.println("You win!");
			} 
		}
	}
}