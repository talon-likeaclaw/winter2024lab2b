/*
 * Assignment #3  2023 - 110
 * Wordle Game Program
 * Author : Talon Dunbar
 * ID : 2131651
 * Data : 6-12-2023 */

import java.util.Random;
import java.util.Scanner;

public class Wordle {

    /*
     * this method will pick a word at random from an array of words
     * the selected word will be the target word (the word the player is guessing)
     * it will initialize an array of 20+ 5-letter words (no repeating characters)
     * it will use a random method to randomly choose a word and return it */
    public static String generateWord() {
        Random random = new Random();
        String[] randomWords = new String[] { "LOVER", "SIGHT", "IMAGE", "STEAM", "CRATE", "FOCUS", "VOICE", "PASTE",
        "URBAN", "BRAKE", "TRAIN", "MIGHT", "FLAKE", "NOISE", "FOCAL", "ZEBRA", "WASTE", "EIGHT", "EXTRA", "DREAM" };
        // creates a random number to select a random word from the array 
        int randomIndex = random.nextInt(randomWords.length);
        String randomString = randomWords[randomIndex];
        return randomString;

    }

    /*
     * this method takes the target word and a character as input
     * it will return true if the character is in the word, and false if it is not */
    public static boolean letterInWord(String wordleString, char guessChar) {
        boolean isInWord = false;
        for (int i = 0; i < wordleString.length(); i++) {
            if (wordleString.charAt(i) == guessChar) {
                isInWord = true;
            }
        }
        return isInWord;
    }

    /*
     * this method takes the target word, a char we want to find in the word, and
     * the position that letter should appear
     * it will return true if the letter is in the word at the specified location
     * and false if it isn't (or out of range) */
    public static boolean letterInSlot(String wordleString, char guessChar, int positionChar) {
        boolean isInSlot = false;
        if (wordleString.charAt(positionChar) == guessChar) {
            isInSlot = true;
        }
        return isInSlot;
    }

    /*
     * this method takes the target word (answer) and the string the user guessed
     * this method will compare the two strings
     * it will return an array of colours based if the letters are in the right
     * position */
    public static String[] guessWord(String answerString, String guessString) {
        String[] colorArray = new String[] { "white", "white", "white", "white", "white" };
        for (int i = 0; i < answerString.length(); i++) {
            // if the letter is in the word but not in the right place change it to yellow
            if (letterInWord(answerString, guessString.charAt(i))) {
                colorArray[i] = "yellow";
                // if the letter is in the right place in the word change it to green
                if (letterInSlot(answerString, guessString.charAt(i), i)) {
                    colorArray[i] = "green";
                }
            }
        }
        return colorArray;
    }

    /*
     * this method takes the 5 letter word and an array of 5 colours
     * each of the 5 letters is printed in its corresponding colour in the array */
    public static void presentResults(String guessString, String[] textColors) {
        String results = "";
        for (int i = 0; i < guessString.length(); i++) {
            char letter = guessString.charAt(i);
            String color = "";
            // if white change text color white
            if (textColors[i] == "white") {
                color = "\u001B[0m";
            // if yellow change text color yellow
            } else if (textColors[i] == "yellow") {
                color = "\u001B[33m";
            // if green turn text color green
            } else {
                color = "\u001B[32m";
            }
            // concatenates color in front of letter with proper space and reinitiliazes as white
            results += color + " " + letter + " " + "\u001B[0m";
        }
        System.out.println(results);
    }

    /*
     * this method asks to user to input a guess word
     * it will validate if the word is 5 letters and ask for a valid guess
     * it will convert the guess to capital letters
     * it will return the user guess */
    public static String readGuess() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Guess a 5 letter word with no repeating characters:");
        String guessString = reader.next();
        // input validation for length
        while (guessString.length() != 5) {
            System.out.println("Please enter a valid 5 letter word:");
            guessString = reader.next();
        }
        // convert user guess to uppercase
        String upperCaseGuessString = guessString.toUpperCase();
        return upperCaseGuessString;
    }

    /*
     * this method takes the target word as an input
     * it asks the user to input a guess word using readGuess
     * the user has 6 attempts to guess the correct word
     * if the user guesses correctly it will print a congratulatory message
     * if they make 6 wrong guesses it will ask the user to try again */
    public static void runGame(String targetString) {
        System.out.println("Let's play a game of Wordle! You have 6 guesses.");
        int loseCount = 6;
        boolean win = false;
        while (loseCount > 0 && !win) {
            // asks the user to input a guess
            String userGuess = readGuess();
            // populates color array with colors depending on guess
            String[] colorArray = guessWord(targetString, userGuess);
            presentResults(userGuess, colorArray);

            loseCount--;
            // if user has used all of their guesses display lose message and tell the user the targetString
            if (loseCount == 0) {
                System.out.println("Sorry, you lost.. Try again! The word was " + targetString);
            }
            // check to see if all of the colors in the array are green
            boolean allGreen = true;
            for (int i = 0; i < colorArray.length; i++) {
                // if a color in the array is not green allGreen becomes false
                if (!(colorArray[i] == "green")) {
                    allGreen = false;
                }
            }
            // if all of the characters are green congratulate user on winning
            if (allGreen) {
                win = true;
                System.out.println("Congrats, you win!!");
            }
            // if user has more guesses remaining let them know how many
            if (loseCount > 0 && !win) {
                System.out.println("You have " + loseCount + " guesses remaining.");
            }
        }
    }
}
